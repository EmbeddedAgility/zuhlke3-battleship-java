package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.List;
import org.scrum.psd.battleship.controller.model.Cell;

public class Ship {
    private List<Cell> shipCells = null;
    private boolean isPlaced;
    private String name;
    private int size;
    private Color color;

    public Ship(String name, int size) {
        shipCells = new ArrayList<>();
        this.name = name;
        this.size = size;
    }

    public int getShipCellCount(){
        return shipCells.size();
    }

    public void putCellInShip(Cell cell){
        if(shipCells.size()<this.size)
        {shipCells.add(cell);}
    }

    public boolean isShipSunk(){
        boolean sunk = true;
        for(Cell cell : shipCells){
            if(!cell.wasShot()){
                sunk = false;
                break;
            }
        }
        return sunk;
    }


    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Cell> getShipCells() {
        return shipCells;
    }
}
