package org.scrum.psd.battleship.controller.model;

import org.scrum.psd.battleship.controller.dto.Ship;

public class Cell{
    private Boolean wasShot = false;
    private Ship ship = null;

    Integer x = null;
    Integer y = null;

    public Cell(Integer x, Integer y){
        this.x=x;
        this.y=y;
    }

    public void setShot(boolean b)
    {
        wasShot = true;
    }

    public boolean wasShot(){
        return wasShot;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
