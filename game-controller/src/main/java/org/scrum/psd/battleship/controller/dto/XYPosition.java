package org.scrum.psd.battleship.controller.dto;

public class XYPosition {
    public int x;
    public int y;

    public XYPosition(int x, int y){
        this.x=x;
        this.y=y;
    }

    public int getRow(){return x;}
    public int getColumn(){return y;}

    public boolean equals(Object other){
        boolean equals = false;
        if(other!=null){
            if(((XYPosition)this).x==((XYPosition)other).x && ((XYPosition)this).y==((XYPosition)other).y)
                equals=true;
        }
        return equals;
    }
}
