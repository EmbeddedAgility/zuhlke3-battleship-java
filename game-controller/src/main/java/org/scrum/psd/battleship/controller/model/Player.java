package org.scrum.psd.battleship.controller.model;

public enum Player {
    //PLAYER_A user
    //PLAYER_B computer
    PLAYER_A, PLAYER_B;
}
