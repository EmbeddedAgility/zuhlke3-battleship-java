package org.scrum.psd.battleship.controller.model;

public class Board{
    Integer xSize;
    Integer ySize;
    Player player;

    Cell[][] space = null;

    public Board(Player player, Integer xSize, Integer ySize){
        this.player = player;
        this.xSize = xSize;
        this.ySize = ySize;

        setNewBoard(xSize,ySize);
    }

    public Cell getCell(Integer x,Integer y){
        return space == null? null : space[x][y];
    }

    private void setNewBoard(Integer xSize, Integer ySize) {
        space = new Cell[xSize][ySize];
        for (int x=0; x<xSize;x++){
            for (int y=0; y<ySize;y++){
                Cell cell = new Cell(x,y);
                space[x][y] = cell;
            }
        }
    }

    public Cell[] getLine(Integer lineNr){
        return space[lineNr];
    }

    public String buildBoardPresentation(){
        StringBuilder boardRepresentation = new StringBuilder();
        boardRepresentation.append("Player " + player);
        Board board = null;
        for (int i=0; i<xSize;i++){
            Cell[] line = getLine(i);
            buildBoardLineRepresentation(i,line,boardRepresentation);
        }

        return boardRepresentation.toString();
    }

    private void buildBoardLineRepresentation(int lineNr, Cell[] line, StringBuilder boardRepresentation) {
        boardRepresentation.append("\n");
        for (int y=0; y<ySize;y++){
            Cell cell = space[lineNr][y];
            String cellFilling = " ";
            if(cell.wasShot()){
                if(cell.getShip()!=null){
                    cellFilling = "x";
                }
                else{
                    cellFilling="-";
                }
            }
            boardRepresentation.append("[" + cellFilling + "]");
        }
    }
}
