package org.scrum.psd.battleship.controller.model;

import org.scrum.psd.battleship.controller.dto.Ship;

public interface Model {

    /**
     * Shoot at the specified cell in the board of the specified player
     *
     * @param player the target player
     * @param x x coordinate of the cel
     * @param y y coordinate of the cell
     */
    public void shootCell(Player player, Integer x,Integer y);

    /**
     * Returns the Cell object at the specified location of the specified player's board
     *
     * @param player the targeted player
     * @param x x coordinate
     * @param y y coordinate
     * @return the specified cell
     */
    public Cell getCell(Player player, Integer x,Integer y);

    /**
     * Associates the given Ship Object to the specified cell (x,y) of the specified player
     *
     * @param ship the ship
     * @param player the player
     * @param x x coordinate
     * @param y y coordinate
     */
    public void putShipInCell(Ship ship, Player player, Integer x, Integer y);

    public Integer getXSize();

    public Integer getYSize();

    public Board getBoardA();

    public Board getBoardB();
}
