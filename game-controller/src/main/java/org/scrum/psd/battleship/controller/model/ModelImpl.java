package org.scrum.psd.battleship.controller.model;

import org.scrum.psd.battleship.controller.dto.Ship;

public class ModelImpl implements Model {

    public Integer xSize;
    public Integer ySize;

    //Boar for Player_A
    Board boardA = null;
    //Boar for Player_B
    Board boardB = null;

    public static void main(String[] args){
        ModelImpl model = new ModelImpl(8,8);

        //place Player A ships
        model.putShipInCell(new Ship("Battleship",3),Player.PLAYER_A,3,2);
        model.putShipInCell(new Ship("Battleship",3),Player.PLAYER_A,1,3);

        //place Player B ships
        model.putShipInCell(new Ship("Battleship",3),Player.PLAYER_B,1,2);
        model.putShipInCell(new Ship("Battleship",3),Player.PLAYER_B,1,3);

        //PLayer A shoots at Player B
        model.shootCell(Player.PLAYER_B,1,2);
        model.shootCell(Player.PLAYER_B,3,3);
        model.shootCell(Player.PLAYER_B,6,5);

        //PLayer B shoots at Player A
        model.shootCell(Player.PLAYER_A,3,2);
        model.shootCell(Player.PLAYER_A,7,1);
        model.shootCell(Player.PLAYER_A,3,6);

        System.out.println(model.buildBoardPresentation(Player.PLAYER_A));
        System.out.println(model.buildBoardPresentation(Player.PLAYER_B));
    }

    public ModelImpl(Integer xSize,Integer ySize){
        this.xSize = xSize;
        this.ySize = ySize;

        boardA = new Board(Player.PLAYER_A,xSize,ySize);
        boardB = new Board(Player.PLAYER_B,xSize,ySize);
    }


    public void shootCell(Player player, Integer x,Integer y){
        Cell cell = getCell(player,x,y);
        cell.setShot(true);
    }

    public Cell getCell(Player player, Integer x,Integer y){
        if(Player.PLAYER_A.equals(player)){
            return boardA.getCell(x,y);
        }
        else{
            return boardB.getCell(x,y);
        }
    }

    @Override
    public void putShipInCell(Ship ship, Player player, Integer x, Integer y) {

    }

    public String buildBoardPresentation(Player player){
        if (Player.PLAYER_A.equals(player)) {
            return boardA.buildBoardPresentation();
        }
        else{
            return boardB.buildBoardPresentation();
        }
    }

    public Integer getXSize() {
        return xSize;
    }

    public Integer getYSize() {
        return ySize;
    }

    public Board getBoardA() {
        return boardA;
    }

    public Board getBoardB() {
        return boardB;
    }

    public void setXSize(Integer xSize) {
        this.xSize = xSize;
    }

    public void setYSize(Integer ySize) {
        this.ySize = ySize;
    }

    public void setBoardA(Board boardA) {
        this.boardA = boardA;
    }

    public void setBoardB(Board boardB) {
        this.boardB = boardB;
    }
}

