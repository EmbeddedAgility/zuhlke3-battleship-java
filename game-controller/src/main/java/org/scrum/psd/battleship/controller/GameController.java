package org.scrum.psd.battleship.controller;

import lombok.RequiredArgsConstructor;
import org.scrum.psd.battleship.controller.dto.*;
import org.scrum.psd.battleship.controller.model.Cell;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public class GameController {

    private final Model model;

    public boolean didShotHit(Player player, XYPosition xyPosition) {
        if(player==null || xyPosition == null){throw new IllegalArgumentException();}

        Cell cell = model.getCell(player, xyPosition.x, xyPosition.y);

        if (cell.wasShot() && cell.getShip()!=null) {
            return true;
        }
        return false;
    }

    public List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE));
    }

    public List<Ship> initializeStealthFleet() {
        return Arrays.asList(
                new Ship("Submarine", 3, Color.CHARTREUSE));
    }

    public boolean isShipValid(Ship ship)
    {
        return ship.getSize() == ship.getShipCellCount();
    }

    public boolean isGameOver(List<Ship> fleet) {
        for (Ship ship: fleet) {
            if (!ship.isShipSunk()) {
                return false;
            }
        }

        return true;
    }
}
