package bdd.features.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.model.Cell;

public class ShipValidationSteps {
    private Ship ship;
    private boolean validationResult;

    private GameController gameController = new GameController(null);

    @Given("^I have a (.*?) ship with (.*?) positions$")
    public void i_have_a_size_ship_with_positions(int size, int positions) throws Throwable {
        ship = new Ship("TestShip",size);

        for (int i = 0; i < positions; i++) {
            ship.putCellInShip(new Cell(0,i));
        }
    }

    @When("^I check if the ship is valid$")
    public void i_check_if_the_ship_is_valid() throws Throwable {
        validationResult = gameController.isShipValid(ship);
    }


    @Then("^the result should be (.*?)$")
    public void the_result_should_be(boolean expectedValidationResult) throws Throwable {
        Assert.assertEquals(expectedValidationResult, validationResult);
    }
}
