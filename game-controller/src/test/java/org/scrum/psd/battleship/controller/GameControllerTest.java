package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.dto.XYPosition;
import org.scrum.psd.battleship.controller.model.Cell;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.ModelImpl;
import org.scrum.psd.battleship.controller.model.Player;

import java.util.Arrays;
import java.util.List;

public class GameControllerTest {
    
    private Model model = new ModelImpl(8, 8);
    private GameController gameController = new GameController(model);
    
    @Test
    @Ignore("To be updated")
    public void testCheckIsHitTrue() {
        List<Ship> ships = gameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            int row = counter;
            for (int i = 0; i < ship.getSize(); i++) {
                model.putShipInCell(ship,Player.PLAYER_A,row,i);
            }
            counter++;
        }

        boolean result = gameController.didShotHit(Player.PLAYER_A, new XYPosition(0,0));

        Assert.assertTrue(result);
    }

    @Test
    @Ignore("To be updated")
    public void testCheckIsHitFalse() {
        List<Ship> ships = gameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            int row = counter;
            for (int i = 0; i < ship.getSize(); i++) {
                model.putShipInCell(ship,Player.PLAYER_A,row,i);
            }
            counter++;
        }

        boolean result = gameController.didShotHit(Player.PLAYER_A, new XYPosition(7,0));

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    @Ignore("To be updated")
    public void testCheckIsHitShipIsNull() {
        gameController.didShotHit(null, new XYPosition(7,0));
    }

    @Test
    public void testIsShipValidFalse() {
        Ship ship = new Ship("TestShip", 3);
        boolean result = gameController.isShipValid(ship);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsShipValidTrue() {
        Ship ship = new Ship("TestShip", 3);
        ship.putCellInShip(new Cell(0,0));
        ship.putCellInShip(new Cell(0,1));
        ship.putCellInShip(new Cell(0,2));

        boolean result = gameController.isShipValid(ship);

        Assert.assertTrue(result);
    }

}
