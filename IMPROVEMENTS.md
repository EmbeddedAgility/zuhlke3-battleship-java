# Improvements list

- Error handling
- Input validation
- Visual improvement
  - Show field status updates
  - Improve input
  - Add field visualisation for deployment of ships
  - Add field visualisation for deployment of the game
- Add random generation option
- Optional instructions
- Improve AI