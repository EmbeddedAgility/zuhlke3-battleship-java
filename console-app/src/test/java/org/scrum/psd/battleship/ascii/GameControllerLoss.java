package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.dto.XYPosition;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameControllerLoss extends GameController {

    private final AtomicBoolean firstCheckPassed = new AtomicBoolean(false);

    public GameControllerLoss(Model model) {
        super(model);
    }

    @Override
    public boolean didShotHit(Player player, XYPosition xyPosition) {
        return true;
    }

    @Override
    public boolean isGameOver(List<Ship> fleet) {
        if (firstCheckPassed.get()) {
            return true;
        } else {
            firstCheckPassed.set(true);
            return false;
        }
    }
}
