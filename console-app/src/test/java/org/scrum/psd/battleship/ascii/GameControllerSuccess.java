package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.model.Model;

import java.util.List;

public class GameControllerSuccess extends GameController {

    public GameControllerSuccess(Model model) {
        super(model);
    }

    @Override
    public boolean isGameOver(List<Ship> fleet) {
        return true;
    }
}
