package org.scrum.psd.battleship.ascii;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.controller.dto.XYPosition;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {

    private final Game game = new Game(null, null,null);

    @Test
    public void testBuildPosition() {
        XYPosition actual = game.buildPosition("A1");
        XYPosition expected = new XYPosition(0,0);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testBuildPosition2() {
        XYPosition actual = game.buildPosition("B1");
        XYPosition expected = new XYPosition(1,0);
        Assertions.assertEquals(expected, actual);
    }
}
