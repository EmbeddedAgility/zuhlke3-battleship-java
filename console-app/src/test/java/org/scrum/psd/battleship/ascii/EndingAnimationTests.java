package org.scrum.psd.battleship.ascii;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.ascii.validator.ValidatorImpl;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.ModelImpl;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

@DisplayNameGeneration(TestNameGenerator.class)
@Execution(ExecutionMode.CONCURRENT)
public class EndingAnimationTests {

    private final Model model = new ModelImpl(8, 8);
    private final GameController gameControllerSuccess = new GameControllerSuccess(model);
    private final GameController gameControllerLoss = new GameControllerLoss(model);

    @ClassRule
    public static final TextFromStandardInputStream gameInput = emptyStandardInputStream();

    @Test
    public void shouldDisplayWinningInformation() {
        gameInput.provideLines("A1", "A2", "A3", "C1");

        new Game(model, gameControllerSuccess, new ValidatorImpl()).runGame();
    }

    @Test
    public void shouldDisplayLosingInformation() {
        gameInput.provideLines("A1", "A2", "A3", "C1");

        new Game(model, gameControllerLoss, new ValidatorImpl()).runGame();
    }

}
