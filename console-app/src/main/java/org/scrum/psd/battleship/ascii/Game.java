package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.ascii.validator.Validator;
import lombok.RequiredArgsConstructor;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.dto.XYPosition;
import org.scrum.psd.battleship.controller.model.Cell;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Logger;

@RequiredArgsConstructor
public class Game {
    Logger logger = Logger.getLogger(Game.class.getName());

    private int xSize = 8;
    private int ySize = 8;

    private GameAnimation gameAnimation;
    private ColoredPrinter console;
    private final Model model;
    private final GameController gameController;
    private final Validator validator;

    List<Ship> myFleet;
    List<Ship> enemyFleet;

    void runGame() {
        gameAnimation = new GameAnimation();
        console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        View.presentGameIntro(console);

        initializeGame();

        startGame();
    }

    private void initializeGame() {
   //     initializeEnemyStealthFleet();
        initializeEnemyFleet();

        initializeMyFleet();
    }


    private void startGame() {
        boolean isGameWon;
        Scanner scanner = new Scanner(System.in);

        View.presentGameStartIntro(console);

        int round = 1;
        do {
            console.println("");

            console.println("=============================================================");
            console.println("                       NEW ROUND   (nr " + round + ")        ");
            console.println("=============================================================");
            View.printMessage(console,"Player, it's your turn");
            View.printMessage(console,"Enter coordinates for your shot :");
            String userInput = scanner.next();

            XYPosition xyPosition = buildPosition(userInput);
            if(xyPosition==null){
                View.printMessage(console,"The specified coordinates were not understood");
                View.printMessage(console,"Please input again");
                continue;
            }

            if (!validator.isWithinBoundaries(model,xyPosition)) {
                View.printMessage(console,"You have entered an invalid position");
                char c = (char)('A' + ySize);
                View.printMessage(console,"Please input a value between A0 and A" + xSize + " and " + c + "0" + c + xSize);
                continue;
            }

            boolean isHit = false;
            if(model.getCell(Player.PLAYER_B,xyPosition.x,xyPosition.y).wasShot()){
                View.printMessage(console,"You shot at this cell before!!!");
                View.printMessage(console,"Pay more attention next time!!");
            }
            else{
                model.shootCell(Player.PLAYER_B,xyPosition.x,xyPosition.y);
                isHit = gameController.didShotHit(Player.PLAYER_B, xyPosition);
                View.printMessage(console,isHit ? "Yeah ! Nice hit !" : "Miss");
                if (isHit) {
                    View.presentHitImage(console);
                    Ship shotShip = model.getCell(Player.PLAYER_B,xyPosition.x,xyPosition.y).getShip();
                    if(shotShip.isShipSunk()){
                        console.println("");
                        View.printMessage(console,"You have sunk a " + shotShip.getName());
                        console.println("");
                        View.presentSinkingIntro(console);
                    }

                    if (gameController.isGameOver(enemyFleet)) {
                        View.printMessage(console,"You win!");
                        isGameWon = true;
                        break;
                    }
                }
            }

            xyPosition = selectComputerShootingPosition(model);
            model.shootCell(Player.PLAYER_A,xyPosition.x,xyPosition.y);
            isHit = gameController.didShotHit(Player.PLAYER_A, xyPosition);
            console.println("\n");
            console.println("\n");
            console.println("\n");
            char c = (char) ('A' +  xyPosition.x);
            View.printMessage(console,String.format("Computer shoot in %s.%s and %s", c, xyPosition.y, isHit ? "hit your ship !" : "miss"));

            if (isHit) {
                View.presentHitImage(console);
                Ship shotShip = model.getCell(Player.PLAYER_A,xyPosition.x,xyPosition.y).getShip();
                if (gameController.isGameOver(myFleet)) {
                    console.println("You loose!");
                    isGameWon = false;
                    break;
                }
                if(shotShip.isShipSunk()){
                    console.println("");
                    View.printMessage(console,"You have lost a " + shotShip.getName());
                    console.println("");
                    View.presentSinkingIntro(console);
                }
            }
            View.presentBoards(console,model);
            round = round +1;
        } while (true);
        gameAnimation.runEndGameAnimation(isGameWon);
    }

    public XYPosition selectComputerShootingPosition(Model model) {
        XYPosition xyPosition = getRandomPosition(model);
        boolean found = false;
        do{
            Cell cell = model.getCell(Player.PLAYER_B,xyPosition.x,xyPosition.y);
            if(!cell.wasShot()){
                found = true;
            }
            else{
                char c = (char) ('A' +  xyPosition.x);
                View.printMessage(console,String.format("Computer chose %s.%s ", c, xyPosition.y, " but it wisely decided to think again."));
                xyPosition = getRandomPosition(model);
            }
        }
        while(!found);
        return xyPosition;
    }

    private XYPosition getRandomPosition(Model model) {

        Random random = new Random();
        int x = random.nextInt(model.getXSize());
        int y = random.nextInt(model.getYSize());
        XYPosition xyPosition = new XYPosition(x,y);
        return xyPosition;
    }

    private void initializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        //List<Ship> myFleet = gameController.initializeShips();
        myFleet = gameController.initializeStealthFleet();
        View.printMessage(console,"Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            console.println("");
            View.printMessage(console,String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                View.printMessage(console,String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                String positionInput = scanner.next();
                XYPosition xyPos = buildPosition(positionInput);
                if(xyPos == null){
                    View.printMessage(console,"The entered values for the position were not understood");
                    View.printMessage(console,"Please enter the value again.");
                    i--;
                    continue;
                }

                if (!validator.isValid(model, Player.PLAYER_A, ship, xyPos)) {
                    View.printMessage(console,"Position is not valid");
                    View.printMessage(console,"Please enter the value again.");
                    displayCurrentShipPositions(ship);
                    i--;
                    continue;
                }

                associateShipAndCell(Player.PLAYER_A, ship,xyPos);
            }
        }
    }

    private void displayCurrentShipPositions(Ship ship) {
        List<Cell> shipCells = ship.getShipCells();
        String msg = "The ships current cells are ";
        for (Cell cell: shipCells){
            char c = (char)('A' + cell.getX());
            msg = msg + c + "."  + (cell.getY()+1) + " ";
        }
        View.printMessage(console,msg);
    }

    public XYPosition buildPosition(String positionInput) {
        XYPosition pos = null;

        if(positionInput==null || positionInput.equals("")){
            return pos;
        }

        positionInput = positionInput.trim();
        positionInput = positionInput.toUpperCase();
        if(positionInput.contains(" ")){
            positionInput = positionInput.replaceAll("\\s+","");
        }
        if(positionInput.length()<getRequiredInputMinLenght() || positionInput.length()>getRequiredInputMaxLenght()){
            return pos;
        }

        int x = positionInput.charAt(0) - 'A' + 1;
        int y = Integer.parseInt(positionInput.substring(1));

        pos = new XYPosition(x-1,y-1);

        return pos;
    }

    private int getRequiredInputMaxLenght(){
        int size = String.valueOf(xSize).length();
        return size*2;
    }

    private int getRequiredInputMinLenght(){
        return 2;
    }

    private void associateShipAndCell(Player player, Ship ship, XYPosition xyPosition) {

        Cell cell = model.getCell(player,xyPosition.x,xyPosition.y);
        ship.putCellInShip(cell);
        cell.setShip(ship);
    }

    private void initializeEnemyFleet() {
        enemyFleet = gameController.initializeShips();

        Ship ship = enemyFleet.get(0);
        putRandomPosition(ship, Player.PLAYER_B, 5);

        ship = enemyFleet.get(1);
        putRandomPosition(ship, Player.PLAYER_B, 4);

        ship = enemyFleet.get(2);
        putRandomPosition(ship, Player.PLAYER_B, 3);


        ship = enemyFleet.get(3);
        putRandomPosition(ship, Player.PLAYER_B, 3);


        ship = enemyFleet.get(4);
        putRandomPosition(ship, Player.PLAYER_B, 2);

    }

    void putRandomPosition(Ship ship, Player player, int times) {
        boolean valid = false;
        for (int i = 0; i < times; i++) {
            while (!valid) {
                XYPosition position = getRandomPosition(model);
                if (validator.isValid(model, player, ship, position)) {
                    valid = true;
                    associateShipAndCell(player, ship, position);
                //    Uncomment this if you would like to cheat
                //    console.println(position.x + " " + position.y);
                }
            }
            valid=false;
        }
    }

    private void initializeEnemyStealthFleet() {
        enemyFleet = gameController.initializeStealthFleet();

        Ship ship = enemyFleet.get(0);
        associateShipAndCell(Player.PLAYER_B,ship,new XYPosition(2,0));
        associateShipAndCell(Player.PLAYER_B,ship,new XYPosition(2,1));
        associateShipAndCell(Player.PLAYER_B,ship,new XYPosition(2,2));
    }
}
