package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.model.Cell;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

public class View {

    public static void presentGameIntro(ColoredPrinter console) {
        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.setForegroundColor(Ansi.FColor.WHITE);
    }

    public static void presentGameStartIntro(ColoredPrinter console) {
        console.print("\033[2J\033[;H");
        presentSmokePlume(console);
    }

    private static void presentSmokePlume(ColoredPrinter console) {
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");
    }

    public static void presentSinkingIntro(ColoredPrinter console) {
        console.setForegroundColor(Ansi.FColor.RED);
        presentSmokePlume(console);
        console.setForegroundColor(Ansi.FColor.WHITE);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void presentHitImage(ColoredPrinter console) {
        beep(console);

        console.println("                \\         .  ./");
        console.println("              \\      .:\" \";'.:..\" \"   /");
        console.println("                  (M^^.^~~:.'\" \").");
        console.println("            -   (/  .    . . \\ \\)  -");
        console.println("               ((| :. ~ ^  :. .|))");
        console.println("            -   (\\- |  \\ /  |  /)  -");
        console.println("                 -\\  \\     /  /-");
        console.println("                   \\  \\   /  /");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void beep(ColoredPrinter console) {
        console.print("\007");
    }

    public static void presentBoards(ColoredPrinter console, Model model)
    {
        console.println("\n");
        int spacerA = 1 + (model.getYSize()*3) - 4;
        int spacerB=  (model.getYSize()*3) - 4 + 4 + (model.getYSize()*3)/2;

        console.print(" Player A");
        printSpacer(console,20);
        console.print("Player B");
        console.print("\n");
        for (int x=0;x<model.getXSize();x++){
            printBoardsLine(console, model, x);
        }
    }

    private static void printBoardsLine(ColoredPrinter console, Model model, int line) {
        console.print(" ");
        printBoardLine(console, model, Player.PLAYER_A, line);
        console.print("    ");
        printBoardLine(console, model, Player.PLAYER_B, line);
        console.println("\n");
    }

    private static void printBoardLine(ColoredPrinter console, Model model, Player player, int line) {
        for (int y = 0; y< model.getYSize(); y++){
            Ansi.FColor colour = Ansi.FColor.WHITE;
            String cellFilling = " ";
            Cell cell = model.getCell(player, line,y);
            if(cell.wasShot()){
                if(cell.getShip()==null) {
                    colour = Ansi.FColor.GREEN;
                    cellFilling="-";
                }
                else {
                    cellFilling = "x";
                    colour = Ansi.FColor.RED;
                }
            }
            console.setForegroundColor(colour);
            console.print("[" + cellFilling + "]");

            console.setForegroundColor(Ansi.FColor.WHITE);
        }

    }

    private static void printSpacer(ColoredPrinter console, int spacer) {
        StringBuilder s = new StringBuilder();
        for (int i=0; i<spacer; i++){
            s.append(" ");
        }
        console.print(s.toString());
    }

    public static void printMessage(ColoredPrinter console, String msg) {
        console.setForegroundColor(Ansi.FColor.GREEN);
        console.println(msg);
        console.setForegroundColor(Ansi.FColor.WHITE);
    }
}
