package org.scrum.psd.battleship.ascii.validator;

import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.dto.XYPosition;
import org.scrum.psd.battleship.controller.model.Cell;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

import java.util.List;

public class ValidatorImpl implements Validator {

    @Override
    public boolean isValid(Model model, Player player, Ship ship, XYPosition xyPosition) {
        if (!isWithinBoundaries(model, xyPosition)) return false;
        if (isPositionAlreadyTaken(model, player, xyPosition.x, xyPosition.y)) return false;
        if (isShipAlreadyFull(ship)) return false;
        if (!isPositionNearToShip(player, ship, xyPosition.x, xyPosition.y)) return false;
        return isInLine(ship, xyPosition.x, xyPosition.y);
    }

    public boolean isWithinBoundaries(Model model, XYPosition xyPosition) {
        boolean ok = true;
        if (xyPosition.x >= model.getXSize() || xyPosition.x < 0 || xyPosition.y >= model.getYSize() || xyPosition.y < 0) {
            ok = false;
        }
        return ok;
    }

    private boolean isPositionAlreadyTaken(Model model, Player player, int x, int y) {
        return model.getCell(player, x, y).getShip() != null;
    }

    private boolean isShipAlreadyFull(Ship ship) {
        if (ship.getShipCellCount() == ship.getSize()) {
            return true;
        }
        return false;
    }

    private boolean isPositionNearToShip(Player player, Ship ship, int x, int y) {
        if (ship.getShipCellCount() == 0) return true;
        List<Cell> cells = ship.getShipCells();
        for (Cell cell : cells) {
            if (cell.getX() + 1 == x && cell.getY() == y
                    || cell.getX() - 1 == x && cell.getY() == y
                    || cell.getX() == x && cell.getY() + 1 == y
                    || cell.getX() == x && cell.getY() - 1 == y)
                return true;
        }
        return false;
    }

    private boolean isInLine(Ship ship, int x, int y) {
        if (ship.getShipCellCount() <= 1) return true;
        List<Cell> cells = ship.getShipCells();
        for (Cell cell : cells) {
            if (cell.getX() != x && cell.getY() != y) return false;
        }
        return true;
    }
}
