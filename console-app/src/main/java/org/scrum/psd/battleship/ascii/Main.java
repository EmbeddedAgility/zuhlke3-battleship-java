package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.ascii.validator.ValidatorImpl;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.ModelImpl;

public class Main {
    public static void main(String[] args) {


        Model model = new ModelImpl(8, 8);
        new Game(model, new GameController(model), new ValidatorImpl()).runGame();
    }
}
