package org.scrum.psd.battleship.ascii.validator;

import org.scrum.psd.battleship.controller.dto.Ship;
import org.scrum.psd.battleship.controller.dto.XYPosition;
import org.scrum.psd.battleship.controller.model.Model;
import org.scrum.psd.battleship.controller.model.Player;

public interface Validator {
    boolean isValid(Model model, Player player, Ship ship, XYPosition positionInput);
    boolean isWithinBoundaries(Model model,XYPosition xyPosition);
}
