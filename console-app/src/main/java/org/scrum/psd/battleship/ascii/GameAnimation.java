package org.scrum.psd.battleship.ascii;

import static java.lang.Thread.sleep;

import lombok.SneakyThrows;

public class GameAnimation {

  private static final String GAME_WON_MESSAGE = "Wooohoooo, YOU WON!!!";
  private static final String GAME_LOST_MESSAGE = "Awwwwwww, you lost :(((";
  private static final String THANK_YOU_MESSAGE = "\n\nThank you for playing! Come back soon!\n\n";

  private final static String ANIMATION_FRAME_1 = "\n\n\n\n\n\n\n\n\n\n\n\n\n"
      + "                                /\\\n"
      + "                               /::\\\n"
      + "                              /::::\\\n"
      + "                ,a_a         /\\::::/\\\n"
      + "               {/ ''\\_      /\\ \\::/\\ \\\n"
      + "               {\\ ,_oo)    /\\ \\ \\/\\ \\ \\\n"
      + "               {/  (_^____/  \\ \\ \\ \\ \\ \\\n"
      + "     .=.      {/ \\___)))*)    \\ \\ \\ \\ \\/\n"
      + "    (.=.`\\   {/   /=;  ~/      \\ \\ \\ \\/\n"
      + "        \\ `\\{/(   \\/\\  /        \\ \\ \\/\n"
      + "         \\  `. `\\  ) )           \\ \\/\n"
      + "          \\    // /_/_            \\/\n"
      + "           '==''---))))\n";

  private final static String ANIMATION_FRAME_2 = "\n\n\n\n\n\n\n\n\n\n\n\n\n"
      + "\n"
      + "                ,a_a\n"
      + "               {/ ''\\_\n"
      + "               {\\ ,_oo)\n"
      + "               {/  (_^_____________________\n"
      + "     .=.      {/ \\___)))*)----------;=====;`\n"
      + "    (.=.`\\   {/   /=;  ~~           |||::::\n"
      + "        \\ `\\{/(   \\/\\               |||::::\n"
      + "         \\  `. `\\  ) )              |||||||\n"
      + "          \\    // /_/_              |||||||\n"
      + "           '==''---))))             |||||||\n";

  @SneakyThrows
  public void runEndGameAnimation(boolean isGameWon) {
    String gameEndStatusMessage = GAME_LOST_MESSAGE;
    if(isGameWon) {
      gameEndStatusMessage = GAME_WON_MESSAGE;
    }

    while (System.in.available() == 0) {

      System.out.println(ANIMATION_FRAME_1);
      System.out.println(gameEndStatusMessage);
      System.out.println(THANK_YOU_MESSAGE);
      sleep(400);

      System.out.println(ANIMATION_FRAME_2);
      System.out.println(gameEndStatusMessage);
      System.out.println(THANK_YOU_MESSAGE);
      sleep(400);
      ;
    }
  }
}
